<?php

return [
    /*################ REDIS CONFIG ##################*/
    'mysql' => [
        'host' => '192.168.10.10',
        'port' => '3306',
        'user' => 'homestead',
        'timeout' => '5',
        'charset' => 'utf8mb4',
        'password' => 'secret',
        'database' => 'new-retail',
        'POOL_MAX_NUM' => '20',
        'POOL_TIME_OUT' => '0.1',
    ],
    /*################ REDIS CONFIG ##################*/
    'redis' => [
        'host' => '127.0.0.1',
        'port' => '6379',
        'auth' => '',
        'POOL_MAX_NUM' => '20',
        'POOL_MIN_NUM' => '5',
        'POOL_TIME_OUT' => '0.1',
    ],
];