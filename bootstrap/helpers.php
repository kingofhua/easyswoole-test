<?php

use EasySwoole\EasySwoole\Config as EasySwooleConfig;

if (!function_exists('config')) {
    if (!function_exists('config')) {
        /**
         * Get / set the specified configuration value.
         *
         * If an array is passed as the key, we will assume you want to set an array of values.
         *
         * @param  array|string|null $key
         * @param  mixed $default
         * @return mixed|\EasySwoole\EasySwoole\Config
         */
        function config($key = null, $default = null)
        {
            if (is_null($key)) {
                return EasySwooleConfig::getInstance()->getConf('database.mysql');
            }

            if (is_array($key)) {
                return EasySwooleConfig::getInstance()->setConf(...$key);
            }

            return EasySwooleConfig::getInstance()->getConf($key) ?: $default;
        }
    }
}
