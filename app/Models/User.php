<?php

namespace App\Models;

use EasySwoole\Mysqli\Mysqli;
use EasySwoole\Mysqli\Config as MysqliConfig;
use EasySwoole\EasySwoole\Config as EasySwooleConfig;

/**
 * Class User
 * @auth: kingofzihua
 * @package App\Models
 */
class User extends Model
{
    protected $table = 'users';

    public function getUserByName($name)
    {
        return self::getDB()->where('name', $name)->get($this->table);
    }

    public static function getMysqlConfig()
    {
        return new MysqliConfig(config('database.mysql'));
    }

    public function getDB()
    {
        $conf = self::getMysqlConfig();

        return new Mysqli($conf);
    }
}