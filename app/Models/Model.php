<?php

namespace App\Models;

use EasySwoole\Mysqli\Mysqli;
use EasySwoole\Mysqli\Config as MysqliConfig;
use EasySwoole\EasySwoole\Config as EasySwooleConfig;

class Model
{
    protected $table;

    public static $db;

    public  $config;

    public function __construct()
    {
        $conf = new MysqliConfig(config('database.mysql'));
        self::$db = new Mysqli($conf);
    }

    public function get()
    {
        
    }
}