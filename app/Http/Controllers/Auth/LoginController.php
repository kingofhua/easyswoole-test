<?php

namespace App\Http\Controllers\Auth;

use EasySwoole\Http\Request;
use EasySwoole\Validate\Validate;
use App\Http\Controllers\Controller;

/**
 * Class LoginController
 * @auth: kingofzihua
 * @desc: authorizations
 * @package App\Http\Controllers\Auth
 */
class LoginController extends Controller
{
    /**
     * @auth: kingofzihua
     * @return bool
     */
    public function login()
    {
        $username = $this->requestParam('username');

        return $this->jsonSuccess($this->request()->getRequestParam(), '登陆成功');
    }

    /**
     * @auth: kingofzihua
     * @param null|string $action
     * @return Validate|null
     */
    protected function validateRule(?string $action): ?Validate
    {
        $validate = new Validate();

        switch ($action) {
            case 'login':
                $validate->addColumn('username', '用户名')->required('不能为空');
                $validate->addColumn('password', '密码')->required('不能为空');
            default :
        }

        return $validate;
    }
}