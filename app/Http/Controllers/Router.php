<?php

namespace App\Http\Controllers;

use FastRoute\RouteCollector;
use EasySwoole\Http\AbstractInterface\AbstractRouter;

/**
 * Class Router
 * @auth: kingofzihua
 * @package App\Http\Controllers;
 */
class Router extends AbstractRouter
{
    /**
     * @auth: kingofzihua
     * @param RouteCollector $routeCollector
     */
    public function initialize(RouteCollector $routeCollector)
    {
        $routeCollector->get('/', '/IndexController/index');
        $routeCollector->get('/jwt', '/IndexController/jwt');
        $routeCollector->get('/database', '/IndexController/database');

        $routeCollector->get('/auth/login', '/Auth/LoginController/login');
    }
}