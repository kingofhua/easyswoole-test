<?php
/**
 * Created by PhpStorm.
 * Author: kingofzihua
 * Date: 2019/7/4
 * Time: 4:20 PM
 */

namespace App\Http\Controllers;

use Firebase\JWT\JWT;
use EasySwoole\Mysqli\Mysqli;
use EasySwoole\Mysqli\Config as MysqliConfig;

/**
 * Class IndexController
 * @auth: kingofzihua
 * @package App\Http\Controllers
 */
class IndexController extends Controller
{
    /**
     * @auth: kingofzihua
     */
    public function index()
    {
        return $this->response()->write('hello word!');
    }

    public function database()
    {
        $conf = new MysqliConfig(config('database.mysql'));
        $db = new Mysqli($conf);
        $data = $db->get('users');//获取一个表的数据
        return $this->writeJson(400, $data);
    }

    /**
     * @auth: kingofzihua
     * @link: https://blog.csdn.net/cjs5202001/article/details/80228937
     * @return bool
     */
    public function jwt()
    {
        $key = "kingofzihua";

        $data = array(
            "iss" => "http://blog.kingofzihua.org",
            "aud" => "http://blog.kingofzihua.org",
            "iat" => time(),
            "nbf" => time(),
            "exp" => time() + 3600 * 24 * 7,
        );

        $token = JWT::encode($data, $key);

        $decoded = JWT::decode($token, $key, array('HS256'));

        return $this->response()->write($token);
    }
}