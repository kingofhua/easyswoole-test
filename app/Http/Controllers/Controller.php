<?php
/**
 * Created by PhpStorm.
 * Author: kingofzihua
 * Date: 2019/7/4
 * Time: 4:20 PM
 */

namespace App\Http\Controllers;

use EasySwoole\Http\AbstractInterface\Controller as BaseController;
use EasySwoole\Http\Message\Status;
use EasySwoole\Validate\Validate;

class Controller extends BaseController
{
    public function index()
    {
    }

    public function requestParam(...$key)
    {
        return $this->request()->getRequestParam(...$key);
    }

    /**
     * @auth: kingofzihua
     * @param $data
     * @param string $message
     * @param int $code
     * @return bool
     */
    public function jsonSuccess($data, $message = '获取成功', $code = 200)
    {
        return $this->writeJson($code, $data, $message);
    }

    /**
     * @auth: kingofzihua
     * @param $code
     * @param $data
     * @param string $message
     * @return bool
     */
    public function jsonError($code, $data, $message = '操作失败')
    {
        return $this->writeJson($code, $data, $message);
    }

    /**
     * 增加验证
     * @auth: kingofzihua
     * @param null|string $action
     * @return bool|null
     */
    protected function onRequest(?string $action): ?bool
    {
        $ret = parent::onRequest($action);
        if ($ret === false) {
            return false;
        }
        $validate = $this->validateRule($action);
        if ($validate) {
            $ret = $this->validate($validate);
            if ($ret == false) {
                $this->writeJson(Status::CODE_BAD_REQUEST, null, "{$validate->getError()->getField()}@{$validate->getError()->getFieldAlias()}:{$validate->getError()->getErrorRuleMsg()}");
                return false;
            }
        }
        return true;
    }

    /**
     * @auth: kingofzihua
     * @param null|string $action
     * @return Validate|null
     */
    protected function validateRule(?string $action): ?Validate
    {
        $validate = new Validate();

        switch ($action) {
            default :
        }

        return $validate;
    }
}